// Creator.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <string.h>
#include <Windows.h>
#include "csprng.h"


TCHAR *randstring(size_t length, BOOL truRand) {
	static char charset[] = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
	TCHAR *randomString = NULL;

	if (length) {
		randomString = malloc((sizeof(TCHAR) * (length + 1)) + 10);
		if (randomString) {
			for (int n = 0; n < length; n++) {
				int key;
					key = rand() % (int)(sizeof(charset) - 2);

				randomString[n] = charset[key];
			}
			randomString[length] = '\0';
		}
	}
	return randomString;
}

int main(int argc, char *argv[])
{
	int status;
	long msPause = 0;
	FILE *fp;
	TCHAR combined[MAX_PATH];
	CSPRNG rng = NULL;
	unsigned long randomDelay = 0;

	if (argc == 2) {
		errno = 0;
		msPause = strtol(argv[1], NULL, 10);
		if (errno != 0) {
			printf("Error! Please enter only number values in ms.");
			return -1;
		}
	}

	if (msPause > 20000)
		msPause = 0;

	rng = csprng_create(rng);
	if (!rng)
		return 1;

	char usableString[MAX_PATH];
	TCHAR base[100] = L"C:\\ProggerTests";
	status = CreateDirectoryW(base, NULL);
	wcscat(base, L"\\Creater");
	status = CreateDirectoryW(base, NULL);
	wcscat(base, L"\\");
	BOOL truRand = FALSE;
	TCHAR *random ;
	
	for (int i = 0; i < 20000; i++) {
		if (msPause == -1) {
			randomDelay = csprng_get_int(rng);
			randomDelay %= 5001;
			Sleep(randomDelay);
			truRand = TRUE;
		}
		else
			Sleep(msPause);
		
		memset(combined, 0, MAX_PATH);
		memset(usableString, 0, MAX_PATH);
		wcscpy(combined, base);

		random = randstring(80, truRand);
		wcscat(random, L".txt");
		wcscat(combined, random);
		wcstombs(usableString, combined, MAX_PATH);
		errno = 0;
		fp = fopen(usableString, "wb");
		fclose(fp);
		free(random);
	}
	return 0;
}

